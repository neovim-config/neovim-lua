local map = vim.api.nvim_set_keymap

vim.g.mapleader = " "
vim.g.maplocalleader = " "

map("n", "<C-c>", "<cmd>nohl<CR>", { noremap = true }) -- Clear highlights -- <c-l> became default in core
map("n", "<leader>,", ":e ~/.config/nvim/init.lua<CR>", { noremap = true })
map("n", "<C-s>", ":luafile %<CR>", { noremap = true })
map("n", "<leader>ls", ":ls<CR>", { noremap = true })
map("n", "<leader>cd", ":cd %:p:h<CR>", { noremap = true })
map("n", "<Leader>w", ":write<CR>", { noremap = true })

-- Undo break points
map("i", ",", ",<c-g>u", { noremap = true })
map("i", ".", ".<c-g>u", { noremap = true })
map("i", "!", "!<c-g>u", { noremap = true })
map("i", "?", "?<c-g>u", { noremap = true })

-- Visual shifting (does not exit Visual mode)
map("v", "<", "<gv", { noremap = true })
map("v", ">", ">gv", { noremap = true })

-- Quit visual mode
map("v", "v", "<Esc>", { noremap = true })

-- Quick command mode
map("n", "<CR>", ":", { noremap = true })
map("n", "<leader>of", ":Telescope oldfiles<CR>", { noremap = true })

-- Bash like
-- map("i", "<C-a>", "<Home>", { noremap = true })
-- map("i", "<C-e>", "<End>", { noremap = true })
-- map("i", "<C-d>", "<Delete>", { noremap = true })

-- Buffer
map("n", "<leader>bn", ":bnext<CR>", { noremap = true })
map("n", "<leader>bp", ":bprevious<CR>", { noremap = true })
map("n", "<leader>bd", ":bdelete<CR>", { noremap = true })

-- szw/vim-maximizer
map("n", "<leader>m", ":MaximizerToggle!<CR>", { noremap = true })

-- Trim trailing whitespace.
map("n", "<leader>tw", ":%s/\\s\\+$//e<cr>", { noremap = true })

--Add move line shortcuts
map("n", "<A-j>", ":m .+1<CR>==", { noremap = true })
map("n", "<A-k>", ":m .-2<CR>==", { noremap = true })
map("i", "<A-j>", "<Esc>:m .+1<CR>==gi", { noremap = true })
map("i", "<A-k>", "<Esc>:m .-2<CR>==gi", { noremap = true })
map("v", "<A-j>", ":m '>+1<CR>gv=gv", { noremap = true })
map("v", "<A-k>", ":m '<-2<CR>gv=gv", { noremap = true })
-- moving text
map("n", "<leader>k", ":m .-2<CR>==", { noremap = true })
map("n", "<leader>j", ":m .+1<CR>==", { noremap = true })
map("i", "<C-j>", "<esc>:m .+2<CR>==", { noremap = true })
map("i", "<C-k>", "<esc>:m .-2<CR>==", { noremap = true })

--Remap escape to leave terminal mode
map("t", "<Esc>", [[<c-\><c-n>]], { noremap = true })

map("n", "<leader>ps", ":PackerSync<CR>", { noremap = true })

-- Toggle to disable mouse mode and indentlines for easier paste
ToggleMouse = function()
  if vim.o.mouse == "a" then
    vim.cmd([[IndentBlanklineDisable]])
    vim.wo.signcolumn = "no"
    vim.o.mouse = "v"
    vim.wo.number = false
    print("Mouse disabled")
  else
    vim.cmd([[IndentBlanklineEnable]])
    vim.wo.signcolumn = "yes"
    vim.o.mouse = "a"
    vim.wo.number = true
    print("Mouse enabled")
  end
end

map("n", "<leader>bm", "<cmd>lua ToggleMouse()<cr>", { noremap = true })

-- custom replace
-- map("n", "<silent> s*", "<cmd>let @/='<'.expand('<cword>').'>'<cr>cgn", { noremap = true })

-- fugitive
map("n", "<Leader>gs", "<cmd>G<CR>", { noremap = true })
map("n", "<Leader>gb", "<cmd>G praise<CR>", { noremap = true })
map("n", "<leader>gc", ":G commit<cr>", { noremap = true })
map("n", "<leader>gd", ":tabe %<cr>:Gvdiffsplit!<CR>", { noremap = true })
map("n", "<leader>gD", ":DiffviewOpen<cr>", { noremap = true })
map("n", "<leader>gm", ":tabe %<cr>:Gvdiffsplit! main<CR>", { noremap = true })
map("n", "<leader>gM", ":DiffviewOpen main<cr>", { noremap = true })
map("n", "<leader>gl", ":Git log<cr>", { noremap = true })
map("n", "<leader>gp", ":Git push<cr>", { noremap = true })
map("n", "<leader>ll", ":diffget LOCAL<cr>", { noremap = true })
map("n", "<leader>rr", ":diffget REMOTE<cr>", { noremap = true })

-- vim-test
-- these "Ctrl mappings" work well when Caps Lock is mapped to Ctrl
map("n", "<silent> t<C-n>", ":TestNearest<CR>", { noremap = true })
map("n", "<silent> t<C-f>", ":TestFile<CR>", { noremap = true })
map("n", "<silent> t<C-s>", ":TestSuite<CR>", { noremap = true })
map("n", "<silent> t<C-l>", ":TestLast<CR>", { noremap = true })
map("n", "<silent> t<C-g>", ":TestVisit<CR>", { noremap = true })

-- Toggle to disable mouse mode and indentlines for easier paste
ToggleMouse = function()
  if vim.o.mouse == "a" then
    vim.cmd([[IndentBlanklineDisable]])
    vim.wo.signcolumn = "no"
    vim.o.mouse = "v"
    vim.wo.number = false
    print("Mouse disabled")
  else
    vim.cmd([[IndentBlanklineEnable]])
    vim.wo.signcolumn = "yes"
    vim.o.mouse = "a"
    vim.wo.number = true
    print("Mouse enabled")
  end
end

map("n", "<leader>tm", "<cmd>lua ToggleMouse()<cr>", { noremap = true })

--Disable numbers in terminal mode
vim.cmd([[
  augroup Terminal
    autocmd!
    au TermOpen * set nonu
  augroup end
]])

--Remap escape to leave terminal mode
vim.api.nvim_set_keymap("t", "<Esc>", [[<c-\><c-n>]], { noremap = true })
